package com.vb.walk.repository;

import java.util.List;
import com.vb.walk.model.Goods;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "goods", path = "goods")
public interface GoodsRepository extends MongoRepository<Goods, String> {

  List<Goods> findByName(@Param("name") String name);
  List<Goods> findByPriceGreaterThan(@Param("price") double price);
  List<Goods> findByPriceLessThan(@Param("price") double price);
  List<Goods> findByPriceBetween(@Param("minPrice") double minPrice, @Param("maxPrice") double maxPrice);
  List<Goods> findByDescription(@Param("description") String description);

}
